#!/usr/bin/env node

const url = "https://campus.uni-freiburg.de:443/qisserver/pages/startFlow.xhtml?_flowId=roomSchedule-flow&roomId=191&roomType=3&currentTermId=1805&time=&navigationPosition=organisation,hisinoneFacilities,hisinoneroomschedule";

const puppeteer = require("puppeteer");

(async () => {
  const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const page = await browser.newPage();
  await page.goto(url);
  console.log(JSON.stringify(await page.evaluate(() => {
    return [...document.querySelectorAll(".column")]
      .map(v => [v.querySelector(".colhead"),
        ...v.querySelectorAll(".schedulePanel")].map(v => v.innerText.trim()));
  })));
  await browser.close();
})();

