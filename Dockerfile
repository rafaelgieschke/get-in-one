from node
workdir /build
copy build-setup .
run ./build-setup
copy . .
run npm install
cmd npm start
